// Copyright (c) 2014 Sylvain Rouquette
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef INCLUDE_LOG_LOG_H_
#define INCLUDE_LOG_LOG_H_

#include "platform/pragma.h"

WARNING_DISABLE_MSVC(4714)  // marked as __forceinline not inlined
WARNING_PUSH
WARNING_DISABLE_MSVC(4100)  // unreferenced formal parameter
WARNING_DISABLE_MSVC(4512)  // assignment operator could not be generated

#include <boost/log/trivial.hpp>

WARNING_POP


#define LOG(lvl)     BOOST_LOG_TRIVIAL(lvl)


namespace kodama { namespace log {

void init();

}  // namespace log
}  // namespace kodama

#endif  // INCLUDE_LOG_LOG_H_
