// Copyright (c) 2014 Sylvain Rouquette
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "log/log.h"
#include "platform/pragma.h"

WARNING_PUSH
WARNING_DISABLE_MSVC(4510)  // default constructor could not be generated
WARNING_DISABLE_MSVC(4610)  // user defined constructor required
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
WARNING_POP


namespace kodama { namespace log {

void init() {
    using namespace boost::log;  // NOLINT
    add_common_attributes();
    add_console_log(std::clog, keywords::format = "%Message%");
    // add_console_log(std::clog, keywords::format = "[%TimeStamp%] %ThreadID%: %Message%");
}

}  // namespace log
}  // namespace kodama
